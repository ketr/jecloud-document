## 3.0.6 (2024-12-24)
### Features
- feature : 预览pdf文件时，传递文件名，用于浏览器插件显示名称
- feature : 预览pdf文件时，如果里面有xss代码，改成下载

## 3.0.5 (2024-10-30)
### Features
- feature : 远程下载升级包
- -feature:解决金仓上线安全问题
- feature : 添加icon图片权限放开

## 3.0.4 (2024-09-19)
### Features
- -feature:增加日志实时输出
- feature : 添加rpc接口，通过文件key，删除文件元数据信息

## 3.0.3 (2024-07-26)
### Features
- feature : rpc接口，返回文件方式修改，避免大文件造成oom
- feature : 添加icon图片权限放开

## 3.0.2 (2024-06-25)
### Features
- feature : 添加icon图片权限放开
- feature : 添加minio依赖
- -feature:增加日志实时输出

## 3.0.1 (2024-06-01)
### Features
- feature : 添加minio配置

### Bug Fixes
- fix : oracle数据库附件属性显示失败问题修复

## 3.0.0 (2024-04-25)
### Features
- -feature:增加证书控制，调整版本至3.0.0
- feat 修改pom: 去掉重复依赖

## 2.2.3 (2024-02-23)
### Features
- -feature:抽离nacos
- -feature:适配nacos

## 2.2.2 (2023-12-29)
### Features
- feat (rpc上传文件接口) : 添加BYTE 文件上传接口

## 2.2.1 (2023-12-15)
### Features
- feat (oracle) : 添加oracle适配

## 2.2.0 (2023-11-15)
### Features
- -feature:修复初始化redis连接池的问题

## 2.1.1 (2023-10-27)
## 2.1.0 (2023-10-20)
## 2.0.9 (2023-09-22)
## 2.0.8 (2023-09-15)
### Features
- feature : 修改redis连接配置

## 1.4.1 (2023-09-08)