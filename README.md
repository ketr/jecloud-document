# 文档服务项目

## 项目简介

JECloud 文档管理模块，用于统一管理平台的文件，文档实现了统一的存储桶管理，用户可以使用多个存储桶管理不同功能的文件。
其主要包括如下功能：

- 存储桶管理
- 文件管理
- 文件元数据管理
- 用户文件上传下载
- 用户文件预览
- 服务间文件上传下载

## 环境依赖

* jdk1.8
* maven
* 请使用官方仓库(http://maven.jepaas.com)，暂不同步jar到中央仓库。


> Maven安装 (http://maven.apache.org/download.cgi)

> Maven学习，请参考[maven-基础](docs/mannual/maven-基础.md)

## 服务模块介绍

- aggregate-tomcat： SpringBoot Tomcat聚合模块
- jecloud-document-facade: 接口门面模块
- jecloud-document-sdk: 消费者模块
- jecloud-document-service: 提供者模块

## 编译部署

使用maven生成对应环境的jar包，例: 生产环境打包
``` shell
//开发环境打包
mvn clean package -Pdev -Dmaven.test.skip=true
//本地开发环境打包
mvn clean package -Plocal -Dmaven.test.skip=true
//测试环境打包
mvn clean package -Ptest -Dmaven.test.skip=true
//生产环境打包
mvn clean package -Pprod -Dmaven.test.skip=true
```

## 开源协议

- [MIT](./LICENSE)
- [平台证书补充协议](./SUPPLEMENTAL_LICENSE.md)

## JECloud主目录
[JECloud 微服务架构低代码平台（点击了解更多）](https://gitee.com/ketr/jecloud.git)