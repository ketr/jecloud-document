/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.document.util;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.document.MetadataEnum;
import com.je.common.base.util.SecurityUserHolder;
import org.apache.commons.lang3.StringUtils;

/**
 * MetadataUtil
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/9/26
 */
public class MetadataUtil {

    public static JSONObject setCreateMetadata() {
        JSONObject metadata = new JSONObject();
        return setCreateMetadata(metadata);
    }

    /**
     * 设置创建人信息
     *
     * @param metadata 元数据对象
     */
    public static JSONObject setCreateMetadata(JSONObject metadata) {
        //补充业务元数据
        if (StringUtils.isNotEmpty(SecurityUserHolder.getCurrentAccount().getTenantId())) {
            metadata.put(MetadataEnum.tenantId.getCode(), SecurityUserHolder.getCurrentAccount().getTenantId());
        }
        if (StringUtils.isNotEmpty(SecurityUserHolder.getCurrentAccountRealUser().getCode())) {
            metadata.put(MetadataEnum.createUser.getCode(), SecurityUserHolder.getCurrentAccountRealUser().getCode());
        }
        if (StringUtils.isNotEmpty(SecurityUserHolder.getCurrentAccountRealUser().getName())) {
            metadata.put(MetadataEnum.createUserName.getCode(), SecurityUserHolder.getCurrentAccountRealUser().getName());
        }
        if (StringUtils.isNotEmpty(SecurityUserHolder.getCurrentAccountRealOrganization().getId())) {
            metadata.put(MetadataEnum.createUserDept.getCode(), SecurityUserHolder.getCurrentAccountRealOrganization().getId());
        }
        if (StringUtils.isNotEmpty(SecurityUserHolder.getCurrentAccountRealOrganization().getName())) {
            metadata.put(MetadataEnum.createUserDeptName.getCode(), SecurityUserHolder.getCurrentAccountRealOrganization().getName());
        }
        return metadata;
    }
}