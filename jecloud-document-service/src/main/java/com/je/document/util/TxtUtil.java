/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.document.util;

import cn.hutool.core.io.IoUtil;
import java.io.IOException;
import java.io.InputStream;

/**
 * 文本工具类
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/9/27
 */
public class TxtUtil {

    /**
     * 探测文本内容字符集内容
     *
     * @param content 文本
     * @return java.lang.String 返回对应的字符集
     */
    public static String detectStr(String content) throws IOException {
        return detectByte(content.getBytes());
    }

    /**
     * 探测文本字节数组字符集内容
     *
     * @param bytes 文本字节数组
     * @return java.lang.String 返回对应的字符集
     */
    public static String detectByte(byte[] bytes) throws IOException {
        return EncodingDetect.detect(bytes);
    }

    /**
     * 探测文本流字符集内容
     *
     * @param inputStream 文本流
     * @return java.lang.String 返回对应的字符集
     */
    public static String detect(InputStream inputStream) throws IOException {
        return EncodingDetect.detect(inputStream);
    }

    /**
     * 文本内容流转字符串-自动识别文本编码格式
     * 
     * @param inputStream 内容流
     * @return java.lang.String
     */
    public static String inputStream2String(InputStream inputStream) throws IOException {
        //inputStream转byte数组
        byte[] bytes = IoUtil.readBytes(inputStream);
        String charset = detectByte(bytes);
        return new String(bytes, charset);
    }

}