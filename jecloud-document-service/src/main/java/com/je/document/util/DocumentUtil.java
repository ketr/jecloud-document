/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.document.util;

import com.je.common.base.DynaBean;
import com.je.common.base.db.JEDatabase;
import com.je.common.base.util.DateUtils;
import com.je.common.base.util.StringUtil;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class DocumentUtil {

    /**
     * @param fileName:文件名称，带后缀
     * @param allowFileType：允许文件上传类型集合
     * @return
     */
    public static boolean checkFileSuffix(String fileName, List<String> allowFileType) {

        if (allowFileType == null || allowFileType.size() == 0) {
            return true;
        }
        if (StringUtil.isEmpty(fileName)) {
            return false;
        }

        String[] arr = fileName.split("\\.");
        String suffix = arr[arr.length - 1];
        if (allowFileType.contains(suffix)) {
            return true;
        } else {
            return false;
        }
    }

    public static String getStringValue(DynaBean dynaBean, String code) {
        String value = dynaBean.getStr(code);
        if (value != null) {
            return value;
        }
        return dynaBean.getStr(code.toUpperCase());
    }

    public static Object getValue(Map<String, Object> map, String code) {
        Object value = map.get(code);
        if (value != null) {
            return value;
        }
        return map.get(code.toUpperCase());
    }

    public static void setDate(DynaBean dynaBean, String code) {
        if (JEDatabase.getCurrentDatabase().equalsIgnoreCase("ORACLE")) {
            dynaBean.set(code, new Date());
        } else {
            dynaBean.set(code, DateUtils.formatDateTime(new Date()));
        }
    }

    public static String getFileExtension(String fileName) {
        if (fileName == null || fileName.trim().isEmpty()) {
            return ""; // 空文件名返回空字符串
        }
        int lastDotIndex = fileName.lastIndexOf('.');
        if (lastDotIndex == -1 || lastDotIndex == fileName.length() - 1) {
            return ""; // 没有点号或点号在最后，说明没有后缀
        }
        return fileName.substring(lastDotIndex + 1).toLowerCase(); // 返回后缀并转换为小写
    }

}
