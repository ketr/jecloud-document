package com.je.document.config;

public class AliyunOssConfig {

    private String url;

    private String basePath;

    private String accessBucket;

    private String accessKey;

    private String secretKey;

    private String permission;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public String getAccessBucket() {
        return accessBucket;
    }

    public void setAccessBucket(String accessBucket) {
        this.accessBucket = accessBucket;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    @Override
    public String toString() {
        return "AliyunOssConfig{" +
                "url='" + url + '\'' +
                ", basePath='" + basePath + '\'' +
                ", accessBucket='" + accessBucket + '\'' +
                ", accessKey='" + accessKey + '\'' +
                ", secretKey='" + secretKey + '\'' +
                ", permission='" + permission + '\'' +
                '}';
    }
}
