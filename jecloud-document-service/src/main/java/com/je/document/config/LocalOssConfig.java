package com.je.document.config;

public class LocalOssConfig {

    private String basePath;

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    @Override
    public String toString() {
        return "LocalOssConfig{" +
                "basePath='" + basePath + '\'' +
                '}';
    }
}
