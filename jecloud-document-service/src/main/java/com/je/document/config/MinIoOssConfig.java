package com.je.document.config;

public class MinIoOssConfig {

    private String address;
    private String accessKey;
    private String secretKey;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("MinioConfig{");
        sb.append("address='").append(address).append('\'');
        sb.append(", accessKey='").append(accessKey).append('\'');
        sb.append(", secretKey='").append(secretKey).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
