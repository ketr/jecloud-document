/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.document.constants;

/**
 * 保存类型枚举
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/8/28
 */
public enum LogTypeEnum {

    /**
     * 上传
     */
    UPLOAD("UPLOAD"),
    /**
     * 下载
     */
    DOWNLOAD("DOWNLOAD"),
    /**
     * 绑定
     */
    BOUNDED("BOUNDED"),
    /**
     * 预览
     */
    PREVIEW("PREVIEW"),
    /**
     * 复制
     */
    COPY("COPY"),
    /**
     * 更新
     */
    UPDATE("UPDATE"),
    /**
     * 删除
     */
    DEL("DEL");

    private String code;

    private LogTypeEnum(String code) {
        this.code = code;
    }

    public static LogTypeEnum getDefault(String code) {
        if (code == null || "".equals(code)) {
            return null;
        }

        for (LogTypeEnum value : LogTypeEnum.values()) {
            if (value.getCode().equals(code)) {
                return value;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

}
