/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.document.constants;

/**
 * 摘要类型枚举
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/8/28
 */
public enum DigestEnum {

    /**
     * MD5
     */
    MD5("MD5"),
    /**
     * SHA-1
     */
    SHA1("SHA-1"),
    /**
     * SHA-256
     */
    SHA256("SHA-256");

    private String code;

    private DigestEnum(String code) {
        this.code = code;
    }

    public static DigestEnum getDefault() {
        return getDefault(null);
    }

    /**
     * 根据code获取枚举，默认MD5
     *
     * @param code
     * @return com.je.paas.document.model.DigestEnum
     */
    public static DigestEnum getDefault(String code) {
        if (code == null || "".equals(code)) {
            return MD5;
        }
        for (DigestEnum value : DigestEnum.values()) {
            if (value.getCode().equals(code)) {
                return value;
            }
        }
        return MD5;
    }

    public String getCode() {
        return code;
    }

}
