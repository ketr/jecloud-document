/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.document.service;

import com.je.common.base.spring.SpringContextHolder;
import com.je.document.config.AliyunOssConfig;
import com.je.document.config.LocalOssConfig;
import com.je.document.config.MinIoOssConfig;
import com.je.document.config.TencentOssConfig;
import com.je.document.constants.SaveTypeEnum;
import com.je.document.exception.DocumentException;
import com.je.document.exception.DocumentExceptionEnum;
import com.je.document.model.Bucket;
import com.je.document.service.impl.FileOperateLocalServiceImpl;
import com.je.document.service.impl.FileOperateService;

import java.util.Date;
import java.util.Properties;

/**
 * 文件工具类工厂
 *
 * @author wangmm@ketr.com.cnx
 * @date 2019/8/29
 */
public class FileOperateFactory {

    /**
     * 获取文件操作工具类实例
     *
     * @param bucketBO 存储空间bucket业务对象
     * @return com.je.paas.document.util.file.file
     */
    public static FileOperateService getInstance(Bucket bucketBO) {
        SaveTypeEnum saveType = SaveTypeEnum.getDefault(bucketBO.getSaveType());
        if (SaveTypeEnum.aliyun == saveType) {
            return SpringContextHolder.getBean("aliyunFileOperater");
        } else if (SaveTypeEnum.local == saveType) {
            return SpringContextHolder.getBean("localFileOperater");
        } else if (SaveTypeEnum.tencent == saveType) {
            return SpringContextHolder.getBean("tencentFileOperater");
        } else if (SaveTypeEnum.minIo == saveType) {
            return SpringContextHolder.getBean("minIoFileOperater");
        } else {
            //拼接配置文件key
            throw new DocumentException("不支持" + saveType + "存储类型！", DocumentExceptionEnum.UNKOWN_ERROR);
        }
    }

    /**
     * 清理spring容器内注册的bean缓存
     *
     * @param bucketBO
     */
    public static void clearBeanCacheByStr(String bucketBO) {
        SaveTypeEnum saveType = SaveTypeEnum.getDefault(bucketBO);
        clearBean(saveType);
    }

    private static void clearBean(SaveTypeEnum saveType) {
        if (SaveTypeEnum.aliyun == saveType) {
            FileOperateService aliyunFileOperater = SpringContextHolder.getBean("aliyunFileOperater");
            AliyunOssConfig aliyunOssConfig = SpringContextHolder.getBean("aliyunOssConfig");
            Properties params = new Properties();
            params.put("oss.aliyun.accessBucket", aliyunOssConfig.getAccessBucket());
            params.put("oss.aliyun.accessKey", aliyunOssConfig.getAccessKey());
            params.put("oss.aliyun.secretKey", aliyunOssConfig.getSecretKey());
            params.put("oss.aliyun.url", aliyunOssConfig.getUrl());
            params.put("oss.aliyun.permission", aliyunOssConfig.getPermission());
            params.put("oss.aliyun.basePath", aliyunOssConfig.getBasePath());
            aliyunFileOperater.reload(params);
        } else if (SaveTypeEnum.local == saveType) {
            FileOperateLocalServiceImpl localFileOperater = SpringContextHolder.getBean("localFileOperater");
            LocalOssConfig localOssConfig = SpringContextHolder.getBean("localOssConfig");
            //从配置文件再次获取
            String basePath = localOssConfig.getBasePath();
            //给这个bean再次赋值
            localFileOperater.setBasePath(basePath);
            localFileOperater.setModifiedTime(new Date());
        } else if (SaveTypeEnum.tencent == saveType) {
            FileOperateService tencentFileOperater = SpringContextHolder.getBean("tencentFileOperater");
            TencentOssConfig tencentOssConfig = SpringContextHolder.getBean("tencentOssConfig");
            Properties properties = new Properties();
            properties.setProperty("", tencentOssConfig.getSecretId());
            properties.setProperty("", tencentOssConfig.getSecretKey());
            properties.setProperty("", tencentOssConfig.getBucketRegion());
            properties.setProperty("", tencentOssConfig.getBucketName());
            properties.setProperty("", tencentOssConfig.getPermission());
            properties.setProperty("", tencentOssConfig.getUrl());
            properties.setProperty("", tencentOssConfig.getBasePath());
            tencentFileOperater.reload(properties);
        }if (SaveTypeEnum.minIo == saveType) {
            FileOperateService minIoFileOperater = SpringContextHolder.getBean("minIoFileOperater");
            MinIoOssConfig minIoOssConfig = SpringContextHolder.getBean("minIoOssConfig");
            Properties params = new Properties();
            params.put("oss.minio.accessKey", minIoOssConfig.getAccessKey());
            params.put("oss.minio.secretKey", minIoOssConfig.getSecretKey());
            params.put("oss.minio.address", minIoOssConfig.getAddress());
            minIoFileOperater.reload(params);
        } else {
            //拼接配置文件key
            throw new DocumentException("不支持" + saveType + "存储类型！", DocumentExceptionEnum.UNKOWN_ERROR);
        }
    }

}