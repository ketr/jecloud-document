/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.document.service.bucket.impl;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.SystemSettingRpcService;
import com.je.common.base.spring.SpringContextHolder;
import com.je.document.constants.SaveTypeEnum;
import com.je.document.entity.DocumentBucketDO;
import com.je.document.reflection.BeanMappingHelper;
import com.je.document.service.bucket.BucketService;
import com.je.document.service.impl.FileOperateLocalServiceImpl;
import com.je.document.service.impl.FileOperateService;
import com.je.document.util.DocumentUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Properties;

@Service
public class BucketServiceImpl implements BucketService {

    @Autowired
    private MetaService metaService;

    @Override
    public DynaBean insertBucket(DynaBean dynaBean) {
        return null;
    }

    @Override
    public DynaBean updateBucket(DynaBean dynaBean) {
        return null;
    }

    @Override
    public DynaBean findBucketBean(String bucket) {
        DynaBean bucketBean;
        //如果bucket为空，在存储桶管理中找存储类型为默认的存储桶
        if (Strings.isNullOrEmpty(bucket)) {
            bucketBean = metaService.selectOne("JE_DOCUMENT_BUCKET", ConditionsWrapper.builder()
                    .eq("is_default_bucket", "1"));
            if (bucketBean == null) {
                bucketBean = metaService.selectOne("JE_DOCUMENT_BUCKET", ConditionsWrapper.builder()
                        .eq("save_type", "local"));
            }
        } else {
            bucketBean = metaService.selectOne("JE_DOCUMENT_BUCKET", ConditionsWrapper.builder()
                    .eq("bucket", bucket));
        }
        SaveTypeEnum saveType = SaveTypeEnum.getDefault(DocumentUtil.getStringValue(bucketBean, "save_type"));
        if (SaveTypeEnum.aliyun == saveType) {
            FileOperateService aliyunFileOperater = SpringContextHolder.getBean("aliyunFileOperater");
            Properties properties = aliyunFileOperater.getProperties();
            bucketBean.setStr("base_path", properties.getProperty("basePath"));
            bucketBean.setStr("permission", properties.getProperty("permission"));
            bucketBean.set("modified_time", properties.getProperty("modifiedTime"));
        } else if (SaveTypeEnum.local == saveType) {
            FileOperateLocalServiceImpl localFileOperater = SpringContextHolder.getBean("localFileOperater");
            bucketBean.setStr("base_path", localFileOperater.getBasePath());
            bucketBean.set("modified_time", localFileOperater.getModifiedTime());
        } else if (SaveTypeEnum.tencent == saveType) {
            FileOperateService tencentFileOperater = SpringContextHolder.getBean("tencentFileOperater");
            Properties properties = tencentFileOperater.getProperties();
            bucketBean.setStr("base_path", properties.getProperty("basePath"));
            bucketBean.setStr("permission", properties.getProperty("permission"));
            bucketBean.set("modified_time", properties.getProperty("modifiedTime"));
        } else if (SaveTypeEnum.minIo == saveType) {
            FileOperateService tencentFileOperater = SpringContextHolder.getBean("minIoFileOperater");
            Properties properties = tencentFileOperater.getProperties();
            bucketBean.setStr("base_path", properties.getProperty("basePath"));
            bucketBean.set("modified_time", properties.getProperty("modifiedTime"));
        }  else {
            //TODO 后续支持oss存储服务
            return null;
        }
        return bucketBean;
    }

    @Override
    public DocumentBucketDO findBucket(String bucket) {
        DynaBean bucketBean = findBucketBean(bucket);
        if (bucketBean == null) {
            return null;
        }
        return BeanMappingHelper.transform(bucketBean.getValues(), DocumentBucketDO.class);
    }

}
