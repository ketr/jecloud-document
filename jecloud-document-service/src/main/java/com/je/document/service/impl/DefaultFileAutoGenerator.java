/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.document.service.impl;

import cn.hutool.core.date.DateUtil;
import com.je.common.base.util.JEUUID;
import com.je.document.constants.SaveTypeEnum;
import com.je.document.model.Bucket;
import com.je.document.service.FileAutoGenerator;
import org.springframework.stereotype.Service;

import java.util.Calendar;

/**
 * 默认命名规则
 *
 * @author wangmm@ketr.com.cn
 * @date 2020/9/27
 */
@Service
public class DefaultFileAutoGenerator implements FileAutoGenerator {

    @Override
    public String pathGenerator(String fileName, String contentType, byte[] byteArray, Bucket bucket) {

        //拼接文件路径
        StringBuilder filePath = new StringBuilder();
//        if (StringUtils.isBlank(bucket.getBasePath())) {
//            filePath.append("/JE");
//        }else{
//            filePath.append(bucket.getBasePath());
//        }
        filePath.append("/document").append(DateUtil.format(Calendar.getInstance().getTime(), "/yyyy/MMdd"));
        return filePath.toString();
    }

    @Override
    public String fileNameGenerator(String fileName, String contentType, byte[] byteArray, Bucket bucket) {
        //生成新名称
        String uuidName = JEUUID.uuid();
        if (bucket.getSaveType().equals(SaveTypeEnum.minIo.getCode())) {
            return uuidName + "/" + fileName;
        }
        //后缀名
        String[] splits = fileName.split("\\.");
        String suffix = splits.length == 1 ? "" : splits[splits.length - 1];
        //拼接文件名
        String fullName = uuidName;
        if (splits.length > 1) {
            fullName += "." + suffix;
        }
        return fullName;
    }

    @Override
    public boolean checkDigest() {
        return true;
    }

}
