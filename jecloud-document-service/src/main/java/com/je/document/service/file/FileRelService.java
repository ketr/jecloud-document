/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.document.service.file;

import com.alibaba.fastjson2.JSONObject;
import com.je.document.entity.DocumentRelDO;
import com.je.ibatis.extension.plugins.pagination.Page;
import java.util.List;
import java.util.Map;

/**
 * 文件引用服务定义
 */
public interface FileRelService {

    /**
     * 根据唯一标识查找文件
     *
     * @param key 文件唯一标识
     * @return java.util.Map<java.lang.String, java.lang.Object>
     */
    Map<String, Object> selectRelMapByKey(String key);

    /**
     * 根据唯一标识查找文件
     *
     * @param key 文件唯一标识
     * @return java.util.Map<java.lang.String, java.lang.Object>
     */
    Map<String, Object> selectRelMapByKey(String key,String version);

    /**
     * 根据唯一标识查找文件
     *
     * @param key 文件唯一标识
     * @return com.je.paas.document.model.entity.DocumentRelDO
     */
    DocumentRelDO selectOneByKey(String key);

    /**
     * 根据元数据查找文件数据
     *
     * @param metadata 元数据
     * @return java.util.List<com.je.paas.document.model.entity.DocumentRelDO>
     */
    List<DocumentRelDO> selectRelByMetadata(JSONObject metadata);

    /**
     * 根据元数据查找文件数据
     *
     * @param metadata 元数据
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
     */
    List<Map<String, Object>> selectFileMapByMetadata(JSONObject metadata);

    /**
     * 根据主键和表名 逻辑删除文件
     *
     * @param tableCode 业务表名
     * @param pkValues  业务主键; 多个主键使用','分隔
     * @param userId    操作用户ID
     */
    default void deleteFilesByPkValues(String tableCode, String pkValues, String userId) {
        deleteFilesByPkValues(tableCode, pkValues, null, null, userId);
    }

    /**
     * 根据主键和表名 逻辑删除文件
     *
     * @param tableCode   业务表名
     * @param pkValues    业务主键; 多个主键使用','分隔
     * @param fieldCodes  指定要删除的字段; 多个使用','分隔
     * @param uploadTypes 指定要删除的上传类型; 多个使用','分隔
     * @param userId      操作用户ID
     */
    void deleteFilesByPkValues(String tableCode, String pkValues, String fieldCodes, String uploadTypes, String userId);

    /**
     * 根据唯一标识集合查找文件
     *
     * @param keys 文件唯一标识集合
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
     */
    List<Map<String, Object>> selectListByKeys(List<String> keys);

    /**
     * 根据文件Id查找 文件引用表
     * @param fileId
     * @return
     */
    List<Map<String, Object>> selectListByFileId(String fileId);

    /**
     * 根据唯一标识和版本号集合查找文件
     *
     * @param keys 文件唯一标识集合
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
     */
    List<Map<String, Object>> selectListByKeysAndVersioin(List<String> keys, String version);

    /**
     * 根据元数据查找文件数据-分页
     *
     * @param page 分页对象
     * @param metadataMap 元数据
     * @param orderBy  排序
     * @return com.je.paas.document.dao.Page
     */
    List<Map<String, Object>> selectFileMapPageByMetadata(Page<Map<String, Object>> page,Map<String,String> metadataMap, String orderBy);

    /**
     * 根据主键查找
     *
     * @param tableCode   业务表名
     * @param pkValues    业务主键; 多个主键使用','分隔
     * @param fieldCodes  指定要删除的字段; 多个使用','分隔
     * @param uploadTypes 指定要删除的上传类型; 多个使用','分隔
     */
    List<Map<String,Object>> selectRelIdsByPkValues(String tableCode, List<String> pkValues,List<String> fieldCodes,List<String> uploadTypes);

    /**
     * 根据引用ID 逻辑删除文件
     *
     * @param relIds
     * @param userId      操作用户ID
     */
    void deleteFilesByRelId(List<String> relIds,String userId);

    /**
     * 根据fileKey逻辑删除文件
     *
     * @param fileKeys 文件key集合
     * @param userId   操作人ID
     */
    void deleteFilesByKey(List<String> fileKeys, String userId);

    /**
     * 根据fileKey逻辑启用文件
     *
     * @param fileKeys 文件key集合
     * @param userId   操作人ID
     */
    void useFilesByKey(List<String> fileKeys, String userId);

    /**
     * 通过fileRelId 物理删除
     * @param fileRelIds
     */
    void deleteFilesByFileRelIds(List<String> fileRelIds);

    /**
     * 通过fileRelId 物理删除文件元数据信息
     * @param fileRelIds
     */
    void deleteMetaDataByFileRelId(List<String> fileRelIds);
    /**
     * 通过fileId 物理删除文件信息
     * @param fileIds
     */
    void deleteFileByFileId(List<String> fileIds);

    List<Map<String, Object>> selectMetaDataByFileKey(String fileKey);
}
