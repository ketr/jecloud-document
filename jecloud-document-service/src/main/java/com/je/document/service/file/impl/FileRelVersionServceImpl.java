/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.document.service.file.impl;

import com.je.common.base.service.MetaService;
import com.je.document.service.file.FileRelVersionServce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

@Service
public class FileRelVersionServceImpl implements FileRelVersionServce {

    @Autowired
    private MetaService metaService;

    @Override
    public List<Map<String, Object>> selectAllVersionFiles(String fileKey) {
        StringBuffer sb = new StringBuffer();
        sb.append("select rel.*,rel.modified_time as modifiedtime,rel.modified_user as modifieduser, rel.create_user as createuser,");
        sb.append("f.je_document_file_id AS fileId,rel.id AS relId, rel.name AS relName, f.content_type AS contentType, f.bucket, f.thumbnail,");
        sb.append("f.file_path AS filePath, f.file_size, rel.file_key AS fileKey, f.full_url AS fullUrl, f.suffix, rel.create_time AS createTime ");
        sb.append("from je_document_rel_version rel INNER JOIN je_document_file f on rel.file_id = f.je_document_file_id and f.is_deleted = 0 ");
        sb.append("where rel.is_deleted=0 and f.is_deleted=0 and file_key = {0}  ORDER BY rel.version asc");
        return metaService.selectSql(sb.toString(), fileKey);
    }

    @Override
    public Map<String, Object> selectVersionFile(String fileKey, int version) {
        StringBuffer sb = new StringBuffer();
        sb.append("select rel.*,rel.modified_time as modifiedtime,rel.modified_user as modifieduser, rel.create_user as createuser,");
        sb.append("f.je_document_file_id AS fileId,rel.id AS relId, rel.name AS relName, f.content_type AS contentType, f.bucket, f.thumbnail,");
        sb.append("f.file_path AS filePath, f.file_size, rel.file_key AS fileKey, f.full_url AS fullUrl, f.suffix, rel.create_time AS createTime ");
        sb.append("from je_document_rel_version rel INNER JOIN je_document_file f on rel.file_id = f.je_document_file_id and f.is_deleted = 0");
        sb.append(" where rel.is_deleted=0 and f.is_deleted=0 and file_key = {0}  and rel.version = {1}");
        List<Map<String, Object>> result = metaService.selectSql(sb.toString(), fileKey, version);
        return result == null || result.isEmpty() ? null : result.get(0);
    }

}
