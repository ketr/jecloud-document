/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.document.service.file;

import com.je.common.base.DynaBean;
import com.je.document.entity.DocumentMetadataDO;
import java.util.List;

public interface FileMetaDataService {

    /**
     * 查找元数据
     *
     * @param relId 文件业务关联ID
     * @param code  业务元数据code
     * @return com.je.paas.document.model.entity.DocumentMetadataDO
     */
    DocumentMetadataDO selectMetadata(String relId, String code);

    /**
     * 查找元数据
     *
     * @param relId 文件业务关联ID
     * @param code  业务元数据code
     * @return com.je.paas.document.model.entity.DocumentMetadataDO
     */
    DynaBean selectMetadataBean(String relId, String code);

    /**
     * 根据文件key查找文件全部元数据
     *
     * @param fileKey 文件唯一标识
     * @return java.util.List<com.je.paas.document.model.entity.DocumentMetadataDO>
     */
    List<DocumentMetadataDO> selectFileMetadata(String fileKey);

    /**
     * 根据relId查找文件全部元数据
     *
     * @param relIds 文件业务关联ID
     * @return java.util.List<com.je.paas.document.model.entity.DocumentMetadataDO>
     */
    List<DocumentMetadataDO> selectFileMetadataById(List<Long> relIds);

    /**
     * 将指定附件的上传类型由临时改为附件
     * (MetadataEnum.uploadType)由(UploadTypeEnum.FORM)改为(UploadTypeEnum.FORM)
     *
     * @param relIds         业务关联表主键
     * @param modifiedUserId 操作人ID
     */
    void updateTempToForm(List<String> relIds,String modifiedUserId);

    /**
     * 复制文件的元数据
     *
     * @param userId       用户ID
     * @param oldRelId     被复制的文件业务关联记录
     * @param newPkValue   新业务主键
     * @param newFuncCode  新功能code
     * @param newTableCode 新业务表code
     * @param newFieldCode 新字段code
     */
    void copyFileRelMetadata(String userId,String oldRelId,String newRelId,String newPkValue,
                             String newFuncCode,String newTableCode,String newFieldCode);

}
