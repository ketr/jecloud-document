/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.document.reflection;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 数据库表反射信息
 *
 * @FileName BeanMapping.java
 * @Author WangMengmeng E-mail:wangmeng6447@163.com
 * @Version 1.0
 */
public class BeanMapping {

    /**
     * 表名称
     */
    private String tableName;

    /**
     * 表主键ID 属性名
     */
    private String keyProperty;

    /**
     * 表主键ID 字段名
     */
    private String keyColumn;

    /**
     * 表字段信息列表
     */
    private List<BeanFieldMapping> fieldList;

    /**
     * 表字段信息列表Map  key-表字段名,value-bean属性名
     */
    private ConcurrentHashMap<String, String> fieldMap;

    public String getTableName() {
        return tableName;
    }


    public void setTableName(String tableName) {
        this.tableName = tableName;
    }


    public String getKeyProperty() {
        return keyProperty;
    }


    public void setKeyProperty(String keyProperty) {
        this.keyProperty = keyProperty;
    }


    public String getKeyColumn() {
        return keyColumn;
    }


    public void setKeyColumn(String keyColumn) {
        this.keyColumn = keyColumn;
    }


    public List<BeanFieldMapping> getFieldList() {
        return fieldList;
    }


    public void setFieldList(List<BeanFieldMapping> fieldList) {
        this.fieldList = fieldList;
    }


    public ConcurrentHashMap<String, String> getFieldMap() {
        return fieldMap;
    }


    public void setFieldMap(ConcurrentHashMap<String, String> fieldMap) {
        this.fieldMap = fieldMap;
    }


    public String colName(String field) {
        if (fieldMap != null) {
            return fieldMap.get(field);
        }
        return null;
    }

}
