/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.document.controller;

import com.je.document.service.DocumentBusService;
import com.je.document.service.DocumentOpenService;
import com.je.servicecomb.CommonRequestService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.File;
import java.io.InputStream;

/**
 * 文档开放接口
 */
@RestController
@RequestMapping(value = "/je/document/open")
public class DocumentOpenController implements CommonRequestService {

    @Autowired
    private DocumentBusService documentBusService;

    @Autowired
    private DocumentOpenService documentOpenService;


    /**
     * 获取原图
     * 开放接口，不需要登录
     * <p>
     * 优先级: 路径参数 > 请求参数
     * 请求方式:
     */
    @RequestMapping(value = "/getOriginalPicture/{code}/{fileKey}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ApiResponses({
            @ApiResponse(code = 200, response = File.class, message = ""),
    })
    public ResponseEntity<InputStream> getOriginalPicture(HttpServletRequest request, @PathVariable(name = "fileKey", required = false) String fileKeyParam, @PathVariable(name = "code", required = false) String code) {
        return documentOpenService.getOriginalPicture(code,fileKeyParam);
    }

    /**
     * 缩略图
     * 开放接口，不需要登录
     * <p>
     * 优先级: 路径参数 > 请求参数
     * 请求方式:
     */
    @RequestMapping(value = "/getThumbnailPicture/{code}/{fileKey}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ApiResponses({
            @ApiResponse(code = 200, response = File.class, message = ""),
    })
    public ResponseEntity<InputStream> getThumbnailPicture(HttpServletRequest request, @PathVariable(name = "fileKey", required = false) String fileKeyParam, @PathVariable(name = "code", required = false) String code) {
        return documentOpenService.getThumbnailPicture(code,fileKeyParam);
    }

    /**
     * app获取原图
     * 开放接口，不需要登录
     * <p>
     * 优先级: 路径参数 > 请求参数
     * 请求方式:
     */
    @RequestMapping(value = "/app/download/{code}/{fileKey}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ApiResponses({
            @ApiResponse(code = 200, response = File.class, message = ""),
    })
    public ResponseEntity<InputStream> getAppOriginalPicture(HttpServletRequest request, @PathVariable(name = "fileKey", required = false) String fileKeyParam, @PathVariable(name = "code", required = false) String code) {
        return documentOpenService.getAppOriginalPicture(code,fileKeyParam);
    }

    /**
     * 缩略图
     * 开放接口，不需要登录
     * <p>
     * 优先级: 路径参数 > 请求参数
     * 请求方式:
     */
    @RequestMapping(value = "/app/preview/{code}/{fileKey}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ApiResponses({
            @ApiResponse(code = 200, response = File.class, message = ""),
    })
    public ResponseEntity<Part> getAppPreviewPicture(HttpServletRequest request, @PathVariable(name = "fileKey", required = false) String fileKeyParam, @PathVariable(name = "code", required = false) String code) {
        return documentOpenService.getAppPreviewPicture(request, code, fileKeyParam);
    }
}
