/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.document.config;

import com.ctrip.framework.apollo.Config;
import com.ctrip.framework.apollo.ConfigService;
import com.je.document.service.impl.FileOperateLocalServiceImpl;
import com.je.document.service.impl.FileOperateService;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import java.util.Date;

/**
 * @program: jecloud-document
 * @author: LIULJ
 * @create: 2021/9/7
 * @description: 文件操作配置初始化
 */
@Configuration
public class DocumentConfig implements EnvironmentAware {

    private Environment environment;

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Bean("aliyunOssConfig")
    public AliyunOssConfig aliyunOssConfig() {
        AliyunOssConfig aliyunOssConfig = new AliyunOssConfig();
        Config config = ConfigService.getConfig("oss");
        aliyunOssConfig.setUrl(config.getProperty("oss.aliyun.url", null));
        aliyunOssConfig.setAccessBucket(config.getProperty("oss.aliyun.accessBucket", null));
        aliyunOssConfig.setAccessKey(config.getProperty("oss.aliyun.accessKey", null));
        aliyunOssConfig.setSecretKey(config.getProperty("oss.aliyun.secretKey", null));
        aliyunOssConfig.setBasePath(config.getProperty("oss.aliyun.basePath", null));
        aliyunOssConfig.setPermission(config.getProperty("oss.aliyun.permission", null));
        return aliyunOssConfig;
    }

    @Bean("minIoOssConfig")
    public MinIoOssConfig minIoOssConfig() {
        MinIoOssConfig minIoConfig = new MinIoOssConfig();
        Config config = ConfigService.getConfig("oss");
        minIoConfig.setAccessKey(config.getProperty("oss.minio.accessKey", null));
        minIoConfig.setSecretKey(config.getProperty("oss.minio.secretKey", null));
        minIoConfig.setAddress(config.getProperty("oss.minio.address", null));
        return minIoConfig;
    }


    @Bean("tencentOssConfig")
    public TencentOssConfig tencentOssConfig() {
        TencentOssConfig tencentOssConfig = new TencentOssConfig();
        Config config = ConfigService.getConfig("oss");
        tencentOssConfig.setBucketName(config.getProperty("oss.tencent.bucketName", null));
        tencentOssConfig.setBucketRegion(config.getProperty("oss.tencent.bucketRegion", null));
        tencentOssConfig.setSecretId(config.getProperty("oss.tencent.secretId", null));
        tencentOssConfig.setSecretKey(config.getProperty("oss.tencent.secretKey", null));
        tencentOssConfig.setUrl(config.getProperty("oss.tencent.url", null));
        tencentOssConfig.setBasePath(config.getProperty("oss.tencent.basePath", null));
        return tencentOssConfig;
    }

    @Bean("localOssConfig")
    public LocalOssConfig localOssConfig() {
        LocalOssConfig localOssConfig = new LocalOssConfig();
        Config config = ConfigService.getConfig("oss");
        String basePath = config.getProperty("oss.local.basePath", null);
        localOssConfig.setBasePath(basePath);
        return localOssConfig;
    }

    @Bean("localFileOperater")
    public FileOperateService localFileOperater(LocalOssConfig localOssConfig) {
        String temDir = environment.getProperty("servicecomb.downloads.directory");
        return new FileOperateLocalServiceImpl(temDir, localOssConfig.getBasePath(), new Date());
    }

}
